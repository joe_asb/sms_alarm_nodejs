CREATE TABLE `users` (
	`id`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
	`email`	TEXT NULL UNIQUE,
	`password`	TEXT NOT NULL UNIQUE,
	`phone`	TEXT NOT NULL UNIQUE,
	`carrier`	TEXT NOT NULL,
    `time_zone` TEXT NOT NULL,
	`validated`	INTEGER NOT NULL 
);

CREATE TABLE `relations` (
	`id_subject`	INTEGER NOT NULL,
	`id_recipient`	INTEGER,
	`validated`	INTEGER,
	`date_created`	INTEGER,
	foreign key(id_subject) references users(id),
	foreign key(id_recipient) references users(id),
	primary key (id_subject, id_recipient)
);

CREATE TABLE `alarms` (
	`id`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
    `id_creator`    INTEGER NOT NULL,
    `date_time_start`   INTEGER NOT NULL,
    `message` TEXT NULL
);

CREATE TABLE `alarm_recipients` (
    `id_alarm` INTEGER NOT NULL,
    `id_user` INTEGER NOT NULL,
    foreign key (id_alarm) references alarms(id),
	foreign key (id_user) references users(id),
	primary key (id_alarm, id_user)
);
