'use strict';
module.exports = (sequelize, DataTypes) => {
  var User = sequelize.define('User', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      allowNull: false,
      autoIncrement: true
    },
    email: {
      type: DataTypes.STRING,
      allowNull: false
    }, 
    password: {
      type: DataTypes.STRING,
      allowNull: true
    },
    phone: {
      type: DataTypes.STRING,
      allowNull: true
    },
    carrier: {
      type: DataTypes.STRING,
      allowNull: true
    },
    time_zone: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    validated: {
      type: DataTypes.INTEGER,
      allowNull: false
    }
  }, {});
  User.associate = function(models) {
    models.User.hasMany(models.Relation, {
      foreignKey: ''
    });
  };
  return User;
};
