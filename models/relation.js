'use strict';
module.exports = (sequelize, DataTypes) => {
  var Relation = sequelize.define('Relation', {
    id_subject: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    id_recipient: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    validated: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    date_created: {
      type: DataTypes.INTEGER,
      allowNull: false
    }
  }, {});
  Relation.associate = function(models) {
    models.Relation.belongsTo(models.User, {
      foreignKey: {
        allowNull: false
      }
    })
  };
  return Relation;
}; 
