module.exports = {
    development: {
      dialect: "sqlite",
      storage: "./sms_alarm.db"
    },
    test: {
      dialect: "sqlite",
      storage: ":memory:"
    },
    production: {
        dialect: "sqlite",
        storage: ":memory:"
    }
  };
